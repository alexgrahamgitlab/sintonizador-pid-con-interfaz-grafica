#Este programa corre una interfaz grafica GUI para la red neuronal que calcula los valores de sintonizacion para controladores PID tipo Pareto.
#NOTA: Para que este programa funcione se debe tener calculadas todas las redes neuronales de cada valor. Es decir se debe correr la red
#para Kp, Ti, Td, y beta. Sin esto no funciona. Para ello se debe usar el programa RedSintonizacion.py y correrlo con los parametros de entrada adecuados.

#Este programa despliega una ventana por la cual se ingresan los parametros de la planta y la interfaz retorna las predicciones de los parametros de 
#sintonizacion. 
# Import
import tensorflow.compat.v1 as tf
tf.disable_v2_behavior() 
import numpy as np
import pandas as pd
#from sklearn.preprocessing import MinMaxScaler
import matplotlib.pyplot as plt
from random import shuffle
import sys
import PySimpleGUI as sg 
import os
from PIL import Image, ImageTk

# imagenes
filename = os.path.abspath("INPUT_FOLDER/EIE.png")
filename2 = os.path.abspath("INPUT_FOLDER/UCR.png")

#tema
sg.change_look_and_feel('TanBlue')

# GUI 
layout = [[sg.Image(filename),sg.Image(filename2)],
         [sg.Text('Sintonizador\n       PID', font='Any 18')],
         [sg.Text("Ingrese Ms:", size=(12,1)), sg.Input(size=(12,1), key= 'MS'),sg.Text("Resultado Kp: ", size=(12,1), key = 'Kp'), sg.Text("", size=(12,1), key= 'result Kp')],
         [sg.Text("Ingrese JdiNorm: ", size=(12,1)), sg.Input(size=(12,1), key = 'Jd'),sg.Text("Resultado Ti: ", size=(12,1), key = "Ti"), sg.Text("", size=(12,1), key= 'result Ti')],
         [sg.Text("Ingrese a: ", size=(12,1)), sg.Input(size=(12,1), key = 'a'),sg.Text("Resultado Td: ", size=(12,1), key = "Td"), sg.Text("", size=(12,1), key= 'result Td')],
         [sg.Text("Ingrese t0: ", size=(12,1)), sg.Input(size=(12,1), key = 't0'),sg.Text("Resultado Beta: ", size=(12,1), key = "b"), sg.Text("", size=(12,1), key= 'result Beta') ],
         [sg.Button("Enter"),sg.Button("Exit")]]
#sg.theme('DarkAmber')
window = sg.Window('Interfaz para Sintonizador PID', layout, element_justification='c', )
#size=(300,500)


#Funcion que llama red sobre valores de entrada
def red_procedure(inputs):
    # Entradas
    Entrada_Ms = inputs[0]
    Entrada_Jdi = inputs[1]
    Entrada_a = inputs[2]
    Entrada_t0 = inputs[3]

    #Entrada_Prueba=np.array([[1.6543,0.11198,0.55102,0.8,0.1]], np.float32)
    Entrada_Prueba=np.array([[Entrada_Ms,Entrada_Jdi,Entrada_a,Entrada_t0]], np.float32) 
    prediction = []
    #valor real Kp = 9.9899
    #valor real Ti = 1.1186
    #valor real Td = 0.24481
    #valor real Beta = 0.66397
    with tf.Session() as sess:
        #Se carga el modelo para Kp
        #se corren los comandos para abrir el model usando el path correcto
        new_saver_Kp = tf.train.import_meta_graph('Red_Kp/modelo.meta') 
        new_saver_Kp.restore(sess,'Red_Kp/modelo')
        #se cargar el grafico que guarda los metadatos
        graph = tf.get_default_graph()
        #se recargan los placeholders 
        X = graph.get_tensor_by_name("X:0")
        Y = graph.get_tensor_by_name("Y:0")
        #se recarga la operacion 
        out = graph.get_tensor_by_name("op_to_restore:0")
        feed_dict = {X:Entrada_Prueba}
        kp_pred = sess.run(out, feed_dict)
        prediction.append(kp_pred.item())


        #Se recarga el modelo para Ti
        new_saver_Ti = tf.train.import_meta_graph('Red_Ti/modelo.meta')
        new_saver_Ti.restore(sess,'Red_Ti/modelo')
        graph = tf.get_default_graph()
        X = graph.get_tensor_by_name("X:0")
        Y = graph.get_tensor_by_name("Y:0")
        out = graph.get_tensor_by_name("op_to_restore:0")
        feed_dict = {X:Entrada_Prueba}
        ti_pred = sess.run(out, feed_dict)
        prediction.append(ti_pred.item())

        #Se recarga el modelo para Td
        new_saver_Td = tf.train.import_meta_graph('Red_Td/modelo.meta')
        new_saver_Td.restore(sess,'Red_Td/modelo')
        graph = tf.get_default_graph()
        X = graph.get_tensor_by_name("X:0")
        Y = graph.get_tensor_by_name("Y:0")
        out = graph.get_tensor_by_name("op_to_restore:0")
        feed_dict = {X:Entrada_Prueba}
        td_pred = sess.run(out, feed_dict)
        prediction.append(td_pred.item())

        #Se recarga el modelo para Beta
        new_saver_Beta = tf.train.import_meta_graph('Red_Beta/modelo.meta')
        new_saver_Beta.restore(sess,'Red_Beta/modelo')
        graph = tf.get_default_graph()
        X = graph.get_tensor_by_name("X:0")
        Y = graph.get_tensor_by_name("Y:0")
        out = graph.get_tensor_by_name("op_to_restore:0")
        feed_dict = {X:Entrada_Prueba}
        beta_pred = sess.run(out, feed_dict)
        prediction.append(beta_pred.item())

        #print( "Las predicciones son: \n>Kp:", prediction[0],"\n>Ti: ",prediction[1],"\n>Td: ",prediction[2],"\n>Beta: ",prediction[3])
        #print(prediction)
        return prediction

while True: 
    event, values = window.read()
    if event is None or event == 'Exit':
        break 
    if event is None or event == 'Enter': 
        inputs = [float(i) for i in values.values()]
        predictions = red_procedure(inputs)
        window['result Kp'].update(round(predictions[0],3))
        
        window['result Ti'].update(round(predictions[1],3))
        
        window['result Td'].update(round(predictions[2],3))
        
        window['result Beta'].update(round(predictions[3],3))
window.close()

