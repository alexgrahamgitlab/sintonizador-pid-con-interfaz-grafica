# Sintonizador PID con Interfaz Gráfica

Este proyecto contiene 2 scripts, uno para entrenar una red neuronal para calcular el valor de Kp, Ti, Td o Beta de una lista de archivos. Y otro para que un usuario pueda ver los resultados de su script de manera sencilla en una interfaz gráfica

Para usar este programa se tiene un requisito:

Tener 4 carpetas llamadas Red_Kp, Red_Ti, Red_Td y Red_Beta en la misma carpeta con el script de entrenamiento llamado RedSintonizacion.py (esto se podria eliminar usando algun comando de creacion de archivos dentro del mismo script)