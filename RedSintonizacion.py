#Este programa es una red neuronal que entrena 1 de 4 variables de sintonizacion para plantas tipo MOO
# NOTA: para que las redes se guarden correctamente se deben crear 4 carpetas llamadas Red_Kp, Red_Ti, Red_Td, Red_Beta y tenerlas contenidas dentro del mismo archivo donde se corra este script
# Es necesario tener un archivo .csv para entrenar la red. Este archivo debe tener las variables en el siguiente orden: a,t0,Kp,Ti,Td,beta,Jdi,Jr,JdiNorm,JrNorm,Ms

# Import
import tensorflow.compat.v1 as tf
tf.disable_v2_behavior() 
import numpy as np
import pandas as pd
from sklearn.preprocessing import MinMaxScaler
import matplotlib.pyplot as plt
from random import shuffle
import sys 
import os 

#A este programa se le deben ingresar los siguientes parametros: Archivo de entrada csv, puede ser cualquiera (shuffle, all_parametros), EPOCHS es la cantidad de iteraciones totales por batch
# probar con valores pequeños primero 1-20, pero depende del batch size que se coloque. VAL es la variable que se piensa entrenar para la red. BATCH_SIZE, poner batch sizes de tamaño 2^n, el rango
# usado en este proyecto fue de 32 a 512. RESULT no importa, pero result en 0 imprime un pdf con los errores por iteracion.  

FILE = sys.argv[1]
EPOCHS = int(sys.argv[2])       
VAL = int(sys.argv[3])      #Valores a ingresar: 2=Kp----3=Ti----4=Td----5=Beta
BATCH_SIZE = int(sys.argv[4])
RESULT = int(sys.argv[5])

# Esta parte de abajo es para hacerle shuffle al archivo de ingreso, se puede descomentar si se desea hacer otro nuevo shuffle a partir de otros datos, sin embargo, es mejor quitar esta parte
# apenas se tengan resultados que sean buenos de entrenamiento.

#with open(FILE) as ip:
#    lines=ip.readlines()
#     header = lines.pop(0)
#     shuffle(lines)
#     lines.insert(0, header)

# with open('shuffled.csv','w') as out:
#     out.writelines(lines)

# Import data
data = pd.read_csv('shuffled.csv')

# Dimensions of dataset
n = data.shape[0]
p = data.shape[1]

# Make data a np.array
data = data.values

#Aqui se define como se dividen los sets de entrenamiento y de pruebas, con 0.7 se tienen 70% para entrenamiento y 30% para pruebas.
def split_train_test(data):
    # Training and test data
    train_start = 0
    train_end = int(np.floor(0.7*n)) 
    test_start = train_end + 1
    test_end = n
    data_train = data[np.arange(train_start, train_end), :]
    data_test = data[np.arange(test_start, test_end), :]
    return {'data_train' :data_train, 'data_test' :data_test}

#Esta funcion es para escalar los datos del archivo a un rango de -1 a 1
def scale(values):
    # Scale data
    scaler = MinMaxScaler(feature_range=(-1, 1))
    scaler.fit(values['data_train'])
    data_train = scaler.transform(values['data_train'])
    data_test = scaler.transform(values['data_test'])
    return {'data_train' :data_train, 'data_test' :data_test}

#Aqui se extraen las variables de interes (Ms,JdrNorm,a,t0)
def build(values, var):
    data_train = values['data_train']
    data_test = values['data_test']
    # Build X and y
    #se usa un indice np array para poder escoger las columnas por separado, el orden de las columnas es Ms,JdiNorm,a,t0
    #es necesario mantener este orden para el script del Gui por lo que si el archivo entrada tiene un orden diferente
    #se deben acomodar los indices de index_col de acuerdo al orden estipulado.
    index_col = np.array([10,8,0,1])
    X_train = data_train[:, index_col]
    X_test = data_test[:, index_col]
    y_train = data_train[:, var]
    y_test = data_test[:, var]
    return {'X_train' :X_train, 'X_test' :X_test, 'y_train' :y_train, 'y_test' :y_test}

#Esta funcion crea el modelo, es la parte mas complicada del programa, leer documentacion para saber bien como funciona.
# A grandes rasgos, se inicializan los pesos y sesgos, ademas se hacen las capas iniciales, escondidas y finales
# La funcion de activacion es relu (a*x+b). Se devuelve la salida que es la salida de la capa final     
def model(X, n_nodes):

    # Initializers
    sigma = 1
    weight_initializer = tf.variance_scaling_initializer(mode="fan_avg", distribution="uniform", scale=sigma)
    bias_initializer = tf.zeros_initializer()

    layers = [None]*len(n_nodes)
    layers[0] = X

    # Hidden weights
    for i in range(1,len(n_nodes)):
        W_hidden = tf.Variable(weight_initializer([n_nodes[i-1], n_nodes[i]]))
        bias_hidden = tf.Variable(bias_initializer([n_nodes[i]]))
        layers[i] = tf.nn.relu(tf.add(tf.matmul(layers[i-1], W_hidden), bias_hidden))
        
    # Output weights
    W_out = tf.Variable(weight_initializer([n_nodes[-1], 1]))
    bias_out = tf.Variable(bias_initializer([1]))
    out = tf.transpose(tf.add(tf.matmul(layers[-1], W_out), bias_out),name="op_to_restore") #nombre que se le pone a la funcion que realiza la red para poder leerla en el archivo de interfaz

    return out

def train(data, epochs, batch_size, val):
    #En esta parte se escoge el nombre del path segun la variable ingresada
    switch = {
        2: "Red_Kp/",
        3: "Red_Ti/",
        4: "Red_Td/",
        5: "Red_Beta/"   
    }

    path = switch[val]
    #Se puede escoger entre cualquiera de estos values, el primero tal vez da mejores resultados pero no funka muy bien en la interfaz, experimentar con esto
    #values = scale(split_train_test(data))
    values = split_train_test(data)
    Xy = build(values, val)
    X_train = Xy['X_train']
    X_test = Xy['X_test']
    y_train = Xy['y_train']
    y_test = Xy['y_test']

    # Number of stocks in training data
    n_stocks = Xy['X_train'].shape[1]

    # Neurons
    n_nodes = [n_stocks, 32, 16, 8]     #3 capas escondidas de 32, 16, y 8 neuronas, se puede cambiar al gusto del programador.

    # Session0
    net = tf.compat.v1.InteractiveSession() #comando para sesion interactiva de tensorflow, permite crear graficas en tiempo real, se puede cambiar a una sesion normal

    # Placeholder
    X = tf.placeholder(dtype=tf.float32, shape=[None, n_stocks], name="X")  #leer que son placeholders en la documentacion, son importantes para abrir el modelo en el otro script
    Y = tf.placeholder(dtype=tf.float32, shape=[None], name="Y")
    out = model(X,n_nodes)

    # Cost function
    mse = tf.reduce_mean(tf.squared_difference(out, Y)) 

    # Optimizer
    opt = tf.train.AdamOptimizer().minimize(mse)

    # Init
    net.run(tf.global_variables_initializer())

    # Add ops to save and restore all the variables.
    saver = tf.train.Saver()


    #mucho de lo que sigue aqui que esta comentado es para crear graficas, ignorar si quiere. 
    # Setup plot
    #plt.ion()
    fig = plt.figure()
    ax1 = fig.add_subplot(111)
    if RESULT:
        line1, = ax1.plot(y_test)
        line2, = ax1.plot(y_test * 0.5)

    # Fit neural net
    mse_train = []
    mse_test = []

    # Run
    #name = "b0.pdf"
    #fig.savefig(name, bbox_inches='tight')

    #estos fors son los que recorren el archivo por batches y entrenan la red.
    for e in range(epochs):

        # Shuffle training data
        shuffle_indices = np.random.permutation(np.arange(len(y_train)))
        X_train = X_train[shuffle_indices]
        y_train = y_train[shuffle_indices]

        # Minibatch training
        for i in range(0, len(y_train)//batch_size):
            start = i 
            batch_x = X_train[start:start + batch_size]
            batch_y = y_train[start:start + batch_size]
            # Run optimizer with batch
            net.run(opt, feed_dict={X: batch_x, Y: batch_y})
            # MSE train 
            mse_train.append(net.run(mse, feed_dict={X: X_train, Y: y_train}))
            # Prediction
            pred = net.run(out, feed_dict={X: X_train})
            if RESULT:
                line2.set_ydata(pred)
                plt.title('Predicciones vs Valores Reales')
                plt.xlabel('Iteraciones', fontsize=18)
                plt.ylabel("Valor del parámetro", fontsize=18)
            else:
                #if (i == 0 or i == 8):
                #    name = "kp_nsca_nsh" + str(e) + "_" +str(i) + ".pdf"
                #    fig.savefig(name, bbox_inches='tight')  

                plt.title('Error del entrenamiento')
                plt.xlabel('Iteraciones', fontsize=18)
                plt.ylabel("Magnitud del error", fontsize=18)
                plt.plot(mse_train, color = '#E74C3C', linewidth=0.1)
                #plt.plot(mse_test, color = '#3498DB',linewidth=0.1)
            
        #fig.show()
        #name = "errornshsc.pdf"
        name_pdf = "Valores_Reales-vs-Valores_Pred.pdf"
        name= path + name_pdf
        fig.savefig(name, bbox_inches='tight')

    pred = net.run(out, feed_dict={X: X_test})      

    #esta parte de aqui es para leer de los errores, se puede quitar
    error_absoluto = abs(pred-y_test)


    mse_test.append(net.run(mse, feed_dict={X: X_test, Y: y_test}))

    print("mse_train promedio : ", np.mean(mse_train))
    print("mse_train minimo : ", np.amin(mse_train))
    print("mse_train maximo : ", np.amax(mse_train))
    print("mse_test : ", mse_test)
    print("MAE_test ", np.mean(error_absoluto))

    print("Ultimas 5 predicciones", pred[::-5])
    print("Ultimos 5 datos reales", y_test[-5:])
    
    print("El valor del error absoluto para los ultimos 5 datos: ", error_absoluto[::-5])

    model_path = path + "modelo"
    saver.save(net,model_path)
  
    return(X, X_test, out)

#llamado a la funcion que activa, entrena, y guarda la red.     
X, X_test, out = train(data,EPOCHS,BATCH_SIZE,VAL)
